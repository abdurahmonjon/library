package postgres

import (
	"context"
	"database/sql"
	"postgres-gin-crud/entity"
)

type Repository struct {
	DB *sql.DB
}

func New(db *sql.DB) *Repository {
	return &Repository{
		DB: db,
	}
}
func (p Repository) DeleteBook(ctx context.Context, id string) error {
	_, err := p.DB.Query("delete from books where id = $1", id)
	if err != nil {
		return err
	}
	return nil
}
func (p Repository) DeleteAuthor(ctx context.Context, id string) error {
	_, err := p.DB.Query("delete from authors where id = $1", id)
	if err != nil {
		return err
	}
	return nil
}
func (p Repository) ListBooksByAuthor(ctx context.Context, authorID string) ([]entity.Book, error) {
	bookList := make([]entity.Book, 10)
	rows, err := p.DB.Query("select * from books where author_id = $1 ", authorID)
	if err != nil {
		return []entity.Book{}, err
	}
	var book entity.Book
	for rows.Next() {
		rows.Scan(&book.ID, &book.Name, &book.Author)
		bookList = append(bookList, book)
	}
	return bookList, nil
}
func (p Repository) CreateBook(ctx context.Context, b entity.Book) error {
	_, err := p.DB.Exec(
		"insert into books values($1,$2,$3)", b.ID, b.Name, b.Author.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (p Repository) CreateAuthor(ctx context.Context, a entity.Author) error {
	_, err := p.DB.Exec(
		"insert into authors values($1,$2)", a.ID, a.Name,
	)
	if err != nil {
		return err
	}
	return nil
}

func (p Repository) GetBook(ctx context.Context, id string) (entity.Book, error) {
	b := entity.Book{}

	row := p.DB.QueryRow(
		"select * from books where id=$1", id,
	)

	if err := row.Scan(&b.ID, &b.Name, &b.Author.ID); err != nil {
		return entity.Book{}, err
	}
	return b, nil
}
func (p Repository) ListAuthors(ctx context.Context) ([]entity.Author, error) {
	var author entity.Author
	authors := make([]entity.Author, 10)
	rows, err := p.DB.Query("SELECT * FROM authors")
	if err != nil {
		return authors, err
	}
	for rows.Next() {
		rows.Scan(&author.ID, &author.Name)
		authors = append(authors, author)
	}
	return authors, nil
}

func (p Repository) GetAuthor(ctx context.Context, ID string) (entity.Author, error) {
	var author entity.Author
	rows, err := p.DB.Query("SELECT * FROM authors WHERE id = $1", ID)
	if err != nil {
		return author, err
	}
	for rows.Next() {
		rows.Scan(&author.ID, &author.Name)
	}
	return author, nil
}
func (p Repository) ListBooks(ctx context.Context) ([]entity.Book, error) {
	var book entity.Book
	books := make([]entity.Book, 10)
	rows, err := p.DB.Query("SELECT * FROM books")
	if err != nil {
		return books, err
	}
	for rows.Next() {
		rows.Scan(&book.ID, &book.Name)
		books = append(books, book)
	}
	return books, nil
}
