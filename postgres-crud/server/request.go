package server

type CreateBookRequest struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	AuthorID string `json:"author_id"`
}
type ListBooks []CreateBookRequest

type CreateAuthorRequest struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
type ListAuthors []CreateAuthorRequest
