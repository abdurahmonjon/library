package server

import (
	"net/http"
	"postgres-gin-crud/entity"

	"github.com/gin-gonic/gin"
)

type Server struct {
	repo Repository
}

func New(repo Repository) Server {
	return Server{
		repo: repo,
	}
}

// CreateBook
// @Summary      Create book
// @Description  creates a book with provided info
// @Tags         books
// @Accept       json
// @Produce      json
// @Param        request body CreateBookRequest true  "Book info"
// @Success      200
// @Failure      400
// @Failure      500
// @Router       /create-book [POST]
func (s Server) CreateBook(c *gin.Context) {
	var request CreateBookRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	b := entity.NewBook(request.Title, entity.Author{
		ID: request.AuthorID,
	})
	if err := s.repo.CreateBook(c.Request.Context(), b); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"ok": true,
	})
}

// CreateAuthor
// @Summary      Create author
// @Description  creates an author with provided info
// @Tags         authors
// @Accept       json
// @Produce      json
// @Param        request body CreateAuthorRequest true  "Author info"
// @Success      200 {object} CreateAuthorResponse
// @Failure      400
// @Failure      500
// @Router       /author [POST]
func (s Server) CreateAuthor(c *gin.Context) {
	var request CreateAuthorRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	}
	a := entity.NewAuthor(request.Name)
	if err := s.repo.CreateAuthor(c.Request.Context(), a); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
}
func (s Server) GetBook(c *gin.Context) {
	query := c.Query("id")
	book, err := s.repo.GetBook(c.Request.Context(), query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	book, err = s.repo.GetBook(c.Request.Context(), book.Author.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Book name":   book.Name,
		"Book author": book.Author.Name,
	})
}
func (s Server) GetAuthor(c *gin.Context) {
	query := c.Query("id")
	author, err := s.repo.GetAuthor(c.Request.Context(), query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Author name": author.Name,
	})
}

// ListBooks
// @Summary      List of books
// @Description  shows lists of books
// @Tags         list-books
// @Accept       json
// @Produce      json
// @Success      200 {object} ListBooks
// @Failure      400
// @Failure      500
// @Router       /list-books [GET]
func (s Server) ListBooks(c *gin.Context) {
	books, err := s.repo.ListBooks(c.Request.Context())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, books)
}
func (s Server) ListAuthors(c *gin.Context) {
	authors, err := s.repo.ListAuthors(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, authors)
}
func (s Server) ListBooksByAuthor(c *gin.Context) {
	id := c.Query("id")
	books, err := s.repo.ListBooksByAuthor(&gin.Context{}, id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, books)
}

func (s Server) DeleteBook(c *gin.Context) {
	id := c.Param("id")
	if err := s.repo.DeleteBook(&gin.Context{}, id); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error": id,
	})
}

func (s Server) DeleteAuthor(c *gin.Context) {
	id := c.Param("id")
	if err := s.repo.DeleteAuthor(&gin.Context{}, id); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"error": id,
	})
}
