package main

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"net"
	"postgres-gin-crud/config"
	"postgres-gin-crud/postgres"
	"postgres-gin-crud/server"
)

// @title           Postgres Crud API
// @version         1.0
// @description     This is a sample server library server.

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io
// @BasePath /v1
func main() {
	cfg, err := config.Load()
	if err != nil {
		panic(err)
	}
	db, err := postgres.Connect(cfg)
	if err != nil {
		panic(err)
	}
	p := postgres.New(db)
	s := server.New(p)
	e := Handle(s)
	e.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	e.Run(net.JoinHostPort(cfg.Host, cfg.Port))
}
