package main

import (
	"github.com/gin-gonic/gin"
)

type ServerRequests interface {
	CreateBook(c *gin.Context)
	CreateAuthor(c *gin.Context)
	GetBook(c *gin.Context)
	GetAuthor(c *gin.Context)
	ListBooks(c *gin.Context)
	ListAuthors(c *gin.Context)
	ListBooksByAuthor(c *gin.Context)
	DeleteBook(c *gin.Context)
	DeleteAuthor(c *gin.Context)
}

func Handle(s ServerRequests) gin.Engine {
	e := gin.Default()
	e.POST("/create-book", s.CreateBook)
	e.POST("/create-author", s.CreateAuthor)
	e.GET("/get-book", s.GetBook)
	e.GET("/get-author", s.GetAuthor)
	e.GET("/list-book", s.ListBooks)
	e.GET("/list-author", s.ListAuthors)
	e.GET("/list-book-by-author", s.ListBooksByAuthor)
	e.DELETE("/delete-book", s.DeleteBook)
	e.DELETE("/delete-author", s.DeleteAuthor)
	return *e
}
